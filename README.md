# ExpiryHelper KeePass Plugin
This is a [plugin](http://keepass.info/plugins.html) for [KeePass 2.x](http://keepass.info/) which assists you in setting custom expiry dates for an entry, saving you from doing date math manually.

## Purpose
The options to expire on a given date or never expire are [already built in](http://keepass.info/help/v2/entry.html) to KeePass. KeePass also have limited set of pre-defined intervals like "1 year", "3 months", etc. _This plugin lets you easily calculate intervals that are not supported by default._

## Usage ##

1. Right-click on an entry.
2. Choose the "Set Expiry..." option on the menu. The main window will pop up.
3. You now have three options:
    1. Expire after a number of days, months and/or years: Select the "After" radio option and enter any combination of days, months and years.
    2. Expire on a certain date: Select the "On" radio option and enter a date.
    3. Never expire: Select the "Never expire" radio option.
4. Click "OK" to save the new expiry, or "Cancel" to close the window without making any changes. If you click "OK", you will notice KeePass has flagged the database as "dirty".

## Screenshots

### Entry Context Menu

![ExpiryHelper Context Menu Screenshot](https://bitbucket.org/alphapenguin/expiryhelper/downloads/ExpiryHelperMenu.png)

### Main Window

![ExpiryHelper Screenshot](https://bitbucket.org/alphapenguin/expiryhelper/downloads/ExpiryHelper.png "Main Window")

## Download and Installation

1. Download [ExpiryHelper.plgx](https://bitbucket.org/alphapenguin/expiryhelper/downloads/ExpiryHelper.plgx)
2. Copy the _ExpiryHelper.plgx_ file into the application folder or a subfolder. The "plugins" folder is a fine choice.
3. To check for updates, use the _Help->Check for Update_ menu in KeePass. New versions can be downloaded from the page you are viewing right now.

See the [plugins page](http://keepass.info/help/v2/plugins.html) on the KeePass main site for more information on installing plugins.

## Building From Source

1. Clone this repository
2. Follow directions on [this page](http://keepass.info/help/v2_dev/plg_index.html).

## Bugs and Missing Features

* The "after" option is always relative to "today".
* It is not in the Entry dialog.

If you find any bugs, feel free to [submit an issue](https://bitbucket.org/alphapenguin/expiryhelper/issues).