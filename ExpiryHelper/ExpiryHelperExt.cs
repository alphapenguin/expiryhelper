﻿using System;
using KeePass.Plugins;
using System.Windows.Forms;
using KeePassLib;

namespace ExpiryHelper
{
    /// <summary>
    /// Simple KeePass plugin to allow easier calculation of expiration dates.
    /// </summary>
    public class ExpiryHelperExt : Plugin, IDisposable
    {
        #region Private fields

        private IPluginHost _host;
        private ToolStripSeparator _mySeparator;
        private ToolStripMenuItem _myMenuItem;

        #endregion

        #region Public

        public override string UpdateUrl
        {
            get { return ExpiryHelper.Properties.Resources.UpdateURL; }
        }

        #endregion

        #region Lifecycle methods

        public override bool Initialize(IPluginHost host)
        {
            if (host == null) throw new ArgumentNullException("host");
            _host = host;

            // Add to the Tools menu
            var tsMenu = _host.MainWindow.EntryContextMenu;
            _mySeparator = new ToolStripSeparator();

            _myMenuItem = new ToolStripMenuItem()
            {
                Text = ExpiryHelper.Properties.Resources.ToolsMenuText
            };
            _myMenuItem.Click += this.OnMenuSelected;
            tsMenu.Items.Add(_mySeparator);
            tsMenu.Items.Add(_myMenuItem);

            // Done initializing
            return true;
        }

        public override void Terminate()
        {
            // Clean up            
            var tsMenu = _host.MainWindow.EntryContextMenu;
            if (_myMenuItem != null)
            {
                _myMenuItem.Click -= this.OnMenuSelected;
                tsMenu.Items.Remove(_myMenuItem);
            }
            if (_mySeparator != null)
            {
                tsMenu.Items.Remove(_mySeparator);
            }
        }

        #endregion

        /// <summary>
        /// Most of the work happens here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMenuSelected(object sender, EventArgs e)
        {
            PwEntry entry = _host.MainWindow.GetSelectedEntry(true);

            if (entry != null)
            {
                var entryTitle = entry.Strings.ReadSafe(PwDefs.TitleField);
                using (var form = new Forms.ExpiryCustomInput())
                {
                    form.Text = string.Format(ExpiryHelper.Properties.Resources.TitleText, entryTitle);
                    // Init dialog with current values
                    if (!entry.Expires)
                    {
                        form.neverExpireRadio.Checked = true;
                        form.expiryDatePicker.Value = entry.ExpiryTime;
                    }
                    else
                    {
                        form.expireOnRadio.Checked = true;
                        form.expiryDatePicker.Value = entry.ExpiryTime;
                        form.UpdateFromDate();
                    }
                    form.UpdateControlState();

                    // Show and await result
                    if (form.ShowDialog(_host.MainWindow) == DialogResult.OK)
                    {
                        // Apply to entry                        
                        if (form.expireOnRadio.Checked)
                        {
                            entry.Expires = true;
                            entry.ExpiryTime = form.expiryDatePicker.Value;
                        }
                        else if (form.expireInRadio.Checked)
                        {
                            entry.Expires = true;
                            entry.ExpiryTime = CalcExpiry((int)form.yearsSpinner.Value, (int)form.monthsSpinner.Value, (int)form.daysSpinner.Value);
                        }
                        else if (form.neverExpireRadio.Checked)
                        {
                            entry.Expires = false;
                        }
                        entry.Touch(true);
                        UpdateUI(entry.ParentGroup);
                    }
                }
            }
        }

        /// <summary>
        /// tell KeePass that changes were made to the entry.
        /// </summary>
        /// <param name="group"></param>
        private void UpdateUI(PwGroup group)
        {
            var f = new Action(() =>
            {
                _host.MainWindow.UpdateUI(false, null, true, group ?? _host.Database.RootGroup, true, null, true);
            });

            if (_host.MainWindow.InvokeRequired)
            {
                f.Invoke();
            }
            else
            {
                _host.MainWindow.Invoke(f);
            }
        }
        
        #region Date Calculation Helpers
        
        static internal DateTime CalcExpiry(int years, int months, int days)
        {
            return DateTime.Today.AddYears(years).AddMonths(months).AddDays(days);
        }

        static internal void CalcYearsMonthDays(DateTime startDate, DateTime endDate, out int years, out int months, out int days)
        {
            days = 0;
            months = 0;
            years = 0;

            // Borrowed and modified from https://github.com/kappy/DateTimeExtensions/blob/master/DateTimeExtensions/NaturalText/DateDiff.cs
            if (endDate < startDate)
            {
                return; // just leave them as zeroes
            }

            var span = endDate.Subtract(startDate);
            int seconds = span.Seconds;
            int minutes = span.Minutes;
            int hours = span.Hours;

            if (endDate.Hour < startDate.Hour)
            {
                days--;
            }
            if (endDate.Day >= startDate.Day)
            {
                days += endDate.Day - startDate.Day;
            }
            else
            {
                months--;
                var daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);
                days += daysInMonth - startDate.Day + endDate.Day;
            }

            if (endDate.Month >= startDate.Month)
            {
                months += endDate.Month - startDate.Month;
            }
            else
            {
                months += 12 - startDate.Month + endDate.Month;
                years--;
            }

            if (endDate.Year >= startDate.Year)
            {
                years += endDate.Year - startDate.Year;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _mySeparator.Dispose();
                    _myMenuItem.Dispose();
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

        #endregion

    }
}
