﻿using System;
using System.Windows.Forms;

namespace ExpiryHelper.Forms
{
    public partial class ExpiryCustomInput : Form
    {
        private bool _cascading = false;

        public ExpiryCustomInput()
        {
            InitializeComponent();
        }

        internal void UpdateFromDelta()
        {
            if (!_cascading)
            {
                _cascading = true;
                expiryDatePicker.Value = ExpiryHelperExt.CalcExpiry((int)yearsSpinner.Value, (int)monthsSpinner.Value, (int)daysSpinner.Value);
                _cascading = false;
            }
        }

        internal void UpdateFromDate()
        {
            if (!_cascading)
            {
                int years, months, days;
                _cascading = true;
                ExpiryHelperExt.CalcYearsMonthDays(DateTime.Today, expiryDatePicker.Value, out years, out months, out days);
                daysSpinner.Value = Math.Max(daysSpinner.Minimum, Math.Min(daysSpinner.Maximum, days));
                monthsSpinner.Value = Math.Max(monthsSpinner.Minimum, Math.Min(monthsSpinner.Maximum, months));
                yearsSpinner.Value = Math.Max(yearsSpinner.Minimum, Math.Min(yearsSpinner.Maximum, years));
                _cascading = false;
            }
        }

        internal void UpdateControlState()
        {
            yearsSpinner.Enabled = false;
            monthsSpinner.Enabled = false;
            daysSpinner.Enabled = false;
            expiryDatePicker.Enabled = false;

            if (expireInRadio.Checked)
            {
                yearsSpinner.Enabled = true;
                monthsSpinner.Enabled = true;
                daysSpinner.Enabled = true;
            }
            else if (expireOnRadio.Checked)
            {
                expiryDatePicker.Enabled = true;
            }
        }

        private void daysSpinner_ValueChanged(object sender, EventArgs e)
        {
            UpdateFromDelta();
        }

        private void monthsSpinner_ValueChanged(object sender, EventArgs e)
        {
            UpdateFromDelta();
        }

        private void yearsSpinner_ValueChanged(object sender, EventArgs e)
        {
            UpdateFromDelta();
        }

        private void expireInRadio_CheckedChanged(object sender, EventArgs e)
        {
            UpdateControlState();
        }

        private void expireOnRadio_CheckedChanged(object sender, EventArgs e)
        {
            UpdateControlState();
        }

        private void neverExpireRadio_CheckedChanged(object sender, EventArgs e)
        {
            UpdateControlState();
        }

        private void expiryDatePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateFromDate();
        }
    }
}
