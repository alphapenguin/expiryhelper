﻿namespace ExpiryHelper.Forms
{
    partial class ExpiryCustomInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExpiryCustomInput));
            this.yearsLabel = new System.Windows.Forms.Label();
            this.monthsLabel = new System.Windows.Forms.Label();
            this.daysLabel = new System.Windows.Forms.Label();
            this.expireInRadio = new System.Windows.Forms.RadioButton();
            this.expireOnRadio = new System.Windows.Forms.RadioButton();
            this.expiryDatePicker = new System.Windows.Forms.DateTimePicker();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.expireGroup = new System.Windows.Forms.GroupBox();
            this.daysSpinner = new System.Windows.Forms.NumericUpDown();
            this.monthsSpinner = new System.Windows.Forms.NumericUpDown();
            this.yearsSpinner = new System.Windows.Forms.NumericUpDown();
            this.neverExpireRadio = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.expireGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daysSpinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthsSpinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearsSpinner)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // yearsLabel
            // 
            resources.ApplyResources(this.yearsLabel, "yearsLabel");
            this.yearsLabel.Name = "yearsLabel";
            // 
            // monthsLabel
            // 
            resources.ApplyResources(this.monthsLabel, "monthsLabel");
            this.monthsLabel.Name = "monthsLabel";
            // 
            // daysLabel
            // 
            resources.ApplyResources(this.daysLabel, "daysLabel");
            this.daysLabel.Name = "daysLabel";
            // 
            // expireInRadio
            // 
            resources.ApplyResources(this.expireInRadio, "expireInRadio");
            this.expireInRadio.Name = "expireInRadio";
            this.expireInRadio.Tag = "IN";
            this.expireInRadio.UseVisualStyleBackColor = true;
            this.expireInRadio.CheckedChanged += new System.EventHandler(this.expireInRadio_CheckedChanged);
            // 
            // expireOnRadio
            // 
            resources.ApplyResources(this.expireOnRadio, "expireOnRadio");
            this.expireOnRadio.Name = "expireOnRadio";
            this.expireOnRadio.Tag = "ON";
            this.expireOnRadio.UseVisualStyleBackColor = true;
            this.expireOnRadio.CheckedChanged += new System.EventHandler(this.expireOnRadio_CheckedChanged);
            // 
            // expiryDatePicker
            // 
            resources.ApplyResources(this.expiryDatePicker, "expiryDatePicker");
            this.expiryDatePicker.Name = "expiryDatePicker";
            this.expiryDatePicker.ValueChanged += new System.EventHandler(this.expiryDatePicker_ValueChanged);
            // 
            // okButton
            // 
            resources.ApplyResources(this.okButton, "okButton");
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Name = "okButton";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // expireGroup
            // 
            this.expireGroup.Controls.Add(this.expireInRadio);
            this.expireGroup.Controls.Add(this.daysLabel);
            this.expireGroup.Controls.Add(this.daysSpinner);
            this.expireGroup.Controls.Add(this.monthsLabel);
            this.expireGroup.Controls.Add(this.monthsSpinner);
            this.expireGroup.Controls.Add(this.yearsLabel);
            this.expireGroup.Controls.Add(this.yearsSpinner);
            this.expireGroup.Controls.Add(this.expireOnRadio);
            this.expireGroup.Controls.Add(this.expiryDatePicker);
            this.expireGroup.Controls.Add(this.neverExpireRadio);
            resources.ApplyResources(this.expireGroup, "expireGroup");
            this.expireGroup.Name = "expireGroup";
            this.expireGroup.TabStop = false;
            // 
            // daysSpinner
            // 
            resources.ApplyResources(this.daysSpinner, "daysSpinner");
            this.daysSpinner.Maximum = new decimal(new int[] {
            366,
            0,
            0,
            0});
            this.daysSpinner.Name = "daysSpinner";
            this.daysSpinner.ValueChanged += new System.EventHandler(this.daysSpinner_ValueChanged);
            // 
            // monthsSpinner
            // 
            resources.ApplyResources(this.monthsSpinner, "monthsSpinner");
            this.monthsSpinner.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.monthsSpinner.Name = "monthsSpinner";
            this.monthsSpinner.ValueChanged += new System.EventHandler(this.monthsSpinner_ValueChanged);
            // 
            // yearsSpinner
            // 
            resources.ApplyResources(this.yearsSpinner, "yearsSpinner");
            this.yearsSpinner.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yearsSpinner.Name = "yearsSpinner";
            this.yearsSpinner.ValueChanged += new System.EventHandler(this.yearsSpinner_ValueChanged);
            // 
            // neverExpireRadio
            // 
            resources.ApplyResources(this.neverExpireRadio, "neverExpireRadio");
            this.neverExpireRadio.Checked = true;
            this.neverExpireRadio.Name = "neverExpireRadio";
            this.neverExpireRadio.TabStop = true;
            this.neverExpireRadio.Tag = "NEVER";
            this.neverExpireRadio.UseVisualStyleBackColor = true;
            this.neverExpireRadio.CheckedChanged += new System.EventHandler(this.neverExpireRadio_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.cancelButton);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // ExpiryCustomInput
            // 
            this.AcceptButton = this.okButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.Controls.Add(this.expireGroup);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "ExpiryCustomInput";
            this.ShowIcon = false;
            this.expireGroup.ResumeLayout(false);
            this.expireGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daysSpinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monthsSpinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearsSpinner)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label yearsLabel;
        internal System.Windows.Forms.Label monthsLabel;
        internal System.Windows.Forms.Label daysLabel;
        internal System.Windows.Forms.RadioButton expireInRadio;
        internal System.Windows.Forms.RadioButton expireOnRadio;
        internal System.Windows.Forms.DateTimePicker expiryDatePicker;
        internal System.Windows.Forms.Button okButton;
        internal System.Windows.Forms.Button cancelButton;
        internal System.Windows.Forms.GroupBox expireGroup;
        internal System.Windows.Forms.NumericUpDown yearsSpinner;
        internal System.Windows.Forms.NumericUpDown monthsSpinner;
        internal System.Windows.Forms.NumericUpDown daysSpinner;
        internal System.Windows.Forms.RadioButton neverExpireRadio;
        internal System.Windows.Forms.Panel panel1;

    }
}